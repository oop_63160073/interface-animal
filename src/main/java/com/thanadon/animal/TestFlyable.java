/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.animal;

/**
 *
 * @author Acer
 */
public class TestFlyable {

    public static void main(String[] args) {
        Plane p1 = new Plane("Twin jett");
        Bird b1 = new Bird("Rose");

        Flyable[] flyable = {p1, b1};
        for (int i = 0; i < flyable.length; i++) {
            if (flyable[i] instanceof Plane) {
                Plane plane = (Plane) flyable[i];
                plane.startEngine();
                plane.run();
                plane.raiseSpeed();
                plane.fly();
                System.out.println();
                continue;
            }
            flyable[i].fly();
            System.out.println();

        }
    }
}
