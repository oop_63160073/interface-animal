/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.animal;

/**
 *
 * @author Acer
 */
public class Snake extends Reptile {

    public Snake(String nickname) {
        super("Snake", nickname, 0);
    }

    @Override
    public void crawl() {
        System.out.println("Snake: " + this.getNickname() + " crawl");

    }

    @Override
    public void eat() {
        System.out.println("Snake: " + this.getNickname() + " eat");
    }

    @Override
    public void move() {
        System.out.println("Snake: " + this.getNickname() + " move");
    }

    @Override
    public void speak() {
        System.out.println("Snake: " + this.getNickname() + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Snake: " + this.getNickname() + " sleep");
    }
}
