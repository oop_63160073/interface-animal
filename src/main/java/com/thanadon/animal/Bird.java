/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.animal;

/**
 *
 * @author Acer
 */
public class Bird extends Poultry {

    public Bird(String nickname) {
        super("Bird", nickname);
    }

    @Override
    public void fly() {
        System.out.println("Bird: " + this.getNickname() + " fly");
    }

    @Override
    public void eat() {
        System.out.println("Bird: " + this.getNickname() + " eat");
    }

    @Override
    public void move() {
        System.out.println("Bird: " + this.getNickname() + " move");
    }

    @Override
    public void speak() {
        System.out.println("Bird: " + this.getNickname() + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Bird: " + this.getNickname() + " sleep");
    }

}
