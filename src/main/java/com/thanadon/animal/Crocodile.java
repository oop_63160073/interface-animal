/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.animal;

/**
 *
 * @author Acer
 */
public class Crocodile extends Reptile {

    public Crocodile(String nickname) {
        super("Crocodile", nickname, 4);
    }

    @Override
    public void crawl() {
        System.out.println("Crocodile: " + this.getNickname() + " crawl");

    }

    @Override
    public void eat() {
        System.out.println("Crocodile: " + this.getNickname() + " eat");
    }

    @Override
    public void move() {
        System.out.println("Crocodile: " + this.getNickname() + " move");
    }

    @Override
    public void speak() {
        System.out.println("Crocodile: " + this.getNickname() + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodile: " + this.getNickname() + " sleep");
    }

}
