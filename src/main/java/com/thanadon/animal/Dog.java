/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.animal;

/**
 *
 * @author Acer
 */
public class Dog extends LandAnimal {

    public Dog(String nickname) {
        super("Dog", nickname, 4);
    }

    @Override
    public void run() {
        System.out.println("Dog: " + this.getNickname() + " run");
    }

    @Override
    public void eat() {
        System.out.println("Dog: " + this.getNickname() + " eat");
    }

    @Override
    public void move() {
        System.out.println("Dog: " + this.getNickname() + " move");
    }

    @Override
    public void speak() {
        System.out.println("Dog: " + this.getNickname() + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Dog: " + this.getNickname() + " sleep");
    }

}
