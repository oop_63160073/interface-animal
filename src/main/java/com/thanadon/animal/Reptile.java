/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.animal;

/**
 *
 * @author Acer
 */
public abstract class Reptile extends Animal {
    
    public Reptile(String name, String nickname, int numberOfleg) {
        super(name, nickname, numberOfleg);
    }
    public abstract void crawl();
    
}
