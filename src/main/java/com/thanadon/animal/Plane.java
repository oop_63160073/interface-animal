/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.animal;

/**
 *
 * @author Acer
 */
public class Plane extends Vahicle implements Runable, Flyable {

    public Plane(String engine) {
        super(engine);
    }
    
    @Override
    public void startEngine() {
        System.out.println("Plane: " + this.getEngine() + " engine started.");
    }

    @Override
    public void stopEngine() {
        System.out.println("Plane: " + this.getEngine() + " engine stoped.");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Plane: " + this.getEngine() + " raise speed.");
    }

    @Override
    public void appyBreak() {
        System.out.println("Plane: " + this.getEngine() + " breaked.");
    }

    @Override
    public void run() {
         System.out.println("Plnae: " + this.getEngine() + " run!!!");
    }

    @Override
    public void fly() {
        System.out.println("Plnae: " + this.getEngine() + " fly!!!");
    }
    
}
