/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.animal;

/**
 *
 * @author Acer
 */
public class Fish extends AquaticAnimal {

    public Fish(String nickname) {
        super("Fish", nickname, 0);
    }

    @Override
    public void swim() {
        System.out.println("Fish: " + this.getNickname() + " swim");
    }

    @Override
    public void eat() {
        System.out.println("Fish: " + this.getNickname() + " eat");
    }

    @Override
    public void move() {
        System.out.println("Fish: " + this.getNickname() + " move");
    }

    @Override
    public void speak() {
        System.out.println("Fish: " + this.getNickname() + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Fish: " + this.getNickname() + " sleep");
    }

}
