/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.animal;

/**
 *
 * @author Acer
 */
public class Cat extends LandAnimal {

    public Cat(String nickname) {
        super("Cat", nickname, 4);
    }

    @Override
    public void run() {
        System.out.println("Cat: " + this.getNickname() + " run");
    }

    @Override
    public void eat() {
        System.out.println("Cat: " + this.getNickname() + " eat");
    }

    @Override
    public void move() {
        System.out.println("Cat: " + this.getNickname() + " move");
    }

    @Override
    public void speak() {
        System.out.println("Cat: " + this.getNickname() + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Cat: " + this.getNickname() + " sleep");
    }

}
