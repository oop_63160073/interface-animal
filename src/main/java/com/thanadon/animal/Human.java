/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.animal;

/**
 *
 * @author Acer
 */
public class Human extends LandAnimal {

    public Human(String nickname) {
        super("Human", nickname, 2);
    }

    @Override
    public void run() {
        System.out.println("Human: " + this.getNickname() + " run");
    }

    @Override
    public void eat() {
        System.out.println("Human: " + this.getNickname() + " eat");
    }

    @Override
    public void move() {
        System.out.println("Human: " + this.getNickname() + " move");
    }

    @Override
    public void speak() {
        System.out.println("Human: " + this.getNickname() + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Human: " + this.getNickname() + " sleep");
    }

}
