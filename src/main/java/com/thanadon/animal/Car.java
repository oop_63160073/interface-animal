/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.animal;

/**
 *
 * @author Acer
 */
public class Car extends Vahicle implements Runable {

    public Car(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Car: " + this.getEngine() + " engine started.");
    }

    @Override
    public void stopEngine() {
        System.out.println("Car: " + this.getEngine() + " engine stoped.");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Car: " + this.getEngine() + " raise speed.");
    }

    @Override
    public void appyBreak() {
        System.out.println("Car: " + this.getEngine() + " breaked.");
    }

    @Override
    public void run() {
         System.out.println("Car: " + this.getEngine() + " run!!!");
    }

}
