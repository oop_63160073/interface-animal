/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.animal;

/**
 *
 * @author Acer
 */
public class Bat extends Poultry {

    public Bat(String nickname) {
        super("Bat", nickname);
    }

    @Override
    public void fly() {
        System.out.println("Bat: " + this.getNickname() + " fly");
    }

    @Override
    public void eat() {
        System.out.println("Bat: " + this.getNickname() + " eat");
    }

    @Override
    public void move() {
        System.out.println("Bat: " + this.getNickname() + " move");
    }

    @Override
    public void speak() {
        System.out.println("Bat: " + this.getNickname() + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Bat: " + this.getNickname() + " sleep");
    }

}
